$("head").append('<script type="text/javascript" src="/js/common.js"></script>');

Session.set('board_editing_name', null);

Template.board.board_id = function(){
    return Session.get('board_id');
};

Template.board.board_name = function(){
    var board = Boards.findOne({_id: Session.get('board_id')});
    if(!board) return null;
    return board.name;
};

Template.board.lists = function(){
    return Lists.find({board_id: Session.get('board_id')});
};

Template.board.editing_name = function(){
  return Session.get('board_editing_name');
};

Template.board.events({
    'click #delete' : function(){
        var id = Session.get('board_id');
        Router.goHome();
        Boards.remove(id);
    },
    'click #board_addList' : function(){
        Lists.insert({board_id: Session.get('board_id'), name:'New List'});
    },
    'click #board_name': function (evt, tmpl) { // start editing list name
        Session.set('board_editing_name', true);
        Meteor.flush(); // force DOM redraw, so we can focus the edit field
        activateInput(tmpl.find("#board_name_input"));
    }
});

Template.board.events(okCancelEvents(
    '#board_name_input',
    {
        ok: function (value) {
            Boards.update(Session.get('board_id'), {$set: {name: value}});
            Session.set('board_editing_name', null);
        },
        cancel: function () {
            Session.set('board_editing_name', null);
        }
    }));


Session.set('list_editListName', null);

Template.list.editListName = function(){
    return Session.get('list_editListName')==this._id;
}

Template.list.cards = function(){
    return Cards.find({list_id:this._id});
}

Template.list.events({
    'click .list_listName': function (evt, tmpl) { // start editing list name
    Session.set('list_editListName', this._id);
    Meteor.flush(); // force DOM redraw, so we can focus the edit field
    activateInput(tmpl.find("#list_name_input"));
    },
    'click .list_delete' :function(){
        Lists.remove(this._id);
    }
})

Template.list.events(okCancelEvents(
    '#list_name_input',
    {
        ok: function (value) {
            Lists.update(this._id, {$set: {name: value}});
            Session.set('list_editListName', null);
        },
        cancel: function () {
            Session.set('list_editListName', null);
        }
    }));

Template.list.events(okCancelEvents(
    '.new_card',
    {
        ok: function (text, evt) {
            Cards.insert({header: text, description: '', board_id: this.board_id, list_id: this._id});
            evt.target.value = "";
        }
    }));