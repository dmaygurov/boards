Template.all_boards.boards = function(){
    return Boards.find({}, {sort:{name:1}});
};

Template.all_boards.board_id = function(){
    return Session.get('board_id');
}

Template.all_boards.editing = function(){
    return Session.equals('editing_boardname', this._id);
};
Template.all_boards.events({
    'mousedown .board_link': function(evt){
        Router.setBoard(this._id);
    },
    'click .board_link': function (evt) {
        // prevent clicks on <a> from refreshing the page.
        evt.preventDefault();
    },
    'dblclick .board_edit': function (evt, tmpl) { // start editing list name
        console.log('dblclick .board_edit');
        Session.set('editing_boardname', this._id);
        Meteor.flush(); // force DOM redraw, so we can focus the edit field
        activateInput(tmpl.find("#board-name-input"));
    }
})

Template.all_boards.events(okCancelEvents(
    '#board-name-input',
    {
        ok: function (value) {
            Boards.update(this._id, {$set: {name: value}});
            Session.set('editing_boardname', null);
        },
        cancel: function () {
            Session.set('editing_boardname', null);
        }
    }));

Template.all_boards.events(okCancelEvents(
    '#new-board',
    {
        ok: function (text, evt) {
            var id = Boards.insert({name: text});
            // TODO Move it to server side
            Lists.insert({board_id:id, name:'To Do'});
            Lists.insert({board_id:id, name:'In Progress'});
            Lists.insert({board_id:id, name:'Done'});
            Router.setBoard(id);
        }
    }));
