Session.set("loginError", false);

Template.login_form.login_failed = function(){
    return Session.get("loginError");
};

Template.login_form.events({
    'submit #login_form' : function(){
        var login = $('#login').val();
        var password = $('#password').val();
        Meteor.loginWithPassword(login, password, function (err) {
            if (err) {
                Session.set("loginError", true);
            }
            else{
                Session.set("loginError", false);
            }
        });
    }
});