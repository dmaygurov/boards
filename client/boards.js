$("head").append('<script type="text/javascript" src="/js/common.js"></script>');

Boards = new Meteor.Collection("boards");
Lists = new Meteor.Collection("lists");
Cards = new Meteor.Collection("cards");

Session.set('board_id', null);
Session.set('card_id', null);

Meteor.subscribe('boards');

Meteor.autosubscribe(function () {
    var board_id = Session.get('board_id');
    if (board_id)
    {
        Meteor.subscribe('lists', board_id);
        Meteor.subscribe('cards', board_id);
    }
});

////////// Main template //////////

Template.body.board_id = function(){
    return Session.get('board_id');
}

Template.navbar.events({
    'mousedown #home': function(evt){
        Router.goHome();
    },
    'click #home': function (evt) {
        // prevent clicks on <a> from refreshing the page.
        evt.preventDefault();
    },
    'mousedown #logout': function(evt){
        Meteor.logout();
    },
    'click #logout': function (evt) {
        // prevent clicks on <a> from refreshing the page.
        evt.preventDefault();
    }
})

////////// Tracking selected board in URL //////////

var BoardsRouter = Backbone.Router.extend({
    routes: {
        "": "main",
        "board/:board_id": "board"
    },
    main: function () {
        Session.set("board_id", null);
        Session.set("card_id", null);
    },
    board: function(board_id){
        Session.set("board_id", board_id);
        Session.set("card_id", null);
    },

    setBoard: function (board_id) {
        this.navigate("board/"+board_id, true);
    },
    goHome: function(){
        this.navigate("/", true);
    }
});

Router = new BoardsRouter;

Meteor.startup(function () {
    Backbone.history.start({pushState: true});
});