// Boards -- {name: String}
Boards = new Meteor.Collection("boards");

Meteor.publish('boards', function (){
    return Boards.find();
});

// Lists - {name: String,
//          board_id: String,
//          order_num: Number}
Lists = new Meteor.Collection("lists");

Meteor.publish('lists', function(board_id){
    return Lists.find({board_id:board_id});
})

// Cards -- {header: String,
//           description: String,
//           board_id: String,
//           list_id: String,
//           order_num: Number}
Cards = new Meteor.Collection("cards");

Meteor.publish('cards', function(board_id){
    return Cards.find({board_id: board_id});
});